package app.entity;

import lombok.*;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@EqualsAndHashCode(callSuper = false)
@Entity
@DiscriminatorValue("CREDIT_CARD")
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class CreditCard extends BillingDetails {

    @Column(name = "card_number")
    private String cardNumber;

    @Column(name = "exp_year")
    int expYear;

    @Column(name = "exp_month")
    int expMonth;
}
