package app.entity;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@EqualsAndHashCode(callSuper = false)
@Entity
@DiscriminatorValue("BANK_ACCOUNT")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BankAccount extends BillingDetails {
    private String account;

    @Column(name = "bank_name")
    private String bankName;
}
