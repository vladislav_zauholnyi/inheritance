package app.repository.interfaces;

import app.entity.Buyer;

public interface BuyerDAO {
    void save(Buyer buyer);

    Buyer get(Long id);
}
