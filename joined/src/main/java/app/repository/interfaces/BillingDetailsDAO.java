package app.repository.interfaces;

import app.entity.BankAccount;
import app.entity.BillingDetails;
import app.entity.CreditCard;

import java.util.List;

public interface BillingDetailsDAO {
    void save(BankAccount bankAccount);

    void save(CreditCard creditCard);

    List<BillingDetails> getBillingDetailsByBuyerID(Long id);
}
