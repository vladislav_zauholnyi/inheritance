package app.repository.impl;

import app.entity.Buyer;
import app.repository.interfaces.BuyerDAO;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.Cacheable;

@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Repository
public class BuyerDAOImpl implements BuyerDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Transactional
    @Override
    public void save(Buyer buyer) {
        Session session = sessionFactory.getCurrentSession();
        session.save(buyer);
    }

    @Transactional(readOnly = true)
    @Override
    public Buyer get(Long id) {
        return (Buyer) sessionFactory
                .getCurrentSession()
                .getNamedQuery("buyerByID")
                .setParameter("id", id)
                .getSingleResult();
    }
}
