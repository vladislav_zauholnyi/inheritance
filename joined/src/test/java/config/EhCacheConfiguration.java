package config;

import net.sf.ehcache.Cache;
import net.sf.ehcache.config.CacheConfiguration;
import org.springframework.cache.ehcache.EhCacheCacheManager;
import org.springframework.cache.ehcache.EhCacheManagerFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Objects;

@Configuration
public class EhCacheConfiguration {

    @Bean
    public EhCacheManagerFactoryBean cacheManagerFactoryBean(){
        return new EhCacheManagerFactoryBean();
    }

    @Bean
    public EhCacheCacheManager cacheManager(){
        CacheConfiguration cacheConfiguration = new CacheConfiguration()
                .eternal(false)                     // if true, timeouts are ignored
                .maxEntriesLocalHeap(10)            // total items that can be stored in cache
                .maxEntriesLocalDisk(100)           // sets the maximum number elements on Disk
                .diskSpoolBufferSizeMB(20)          // sets the disk spool size, which is used to buffer writes to the DiskStore
                .timeToIdleSeconds(300)             // time since last accessed before item is marked for removal
                .timeToLiveSeconds(600)             // time since inserted before item is marked for removal
                .memoryStoreEvictionPolicy("LRU")   // Least Recently Used
                .transactionalMode("off")           // disables transactional mode
                .name("buyer");                     // sets a name, which we pass to @Cacheable annotation

        Cache cache = new Cache(cacheConfiguration);
        Objects
                .requireNonNull(cacheManagerFactoryBean().getObject(), "EhCacheManagerFactoryBean can't be NULL!")
                .addCache(cache);

        return new EhCacheCacheManager(Objects
                .requireNonNull(cacheManagerFactoryBean().getObject(), "EhCacheManagerFactoryBean can't be NULL!"));
    }
}
