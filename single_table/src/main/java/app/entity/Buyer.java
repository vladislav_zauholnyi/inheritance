package app.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@NamedQueries({@NamedQuery(name = "buyerByID", query = "from Buyer where id=:id")})
public class Buyer {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "billing_details")
    @OneToMany(mappedBy = "buyer",
            cascade = CascadeType.ALL,
            orphanRemoval = true)
    private List<BillingDetails> billingDetails;
}
