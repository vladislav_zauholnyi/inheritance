package app.repository.impl;

import app.entity.BankAccount;
import app.entity.BillingDetails;
import app.entity.CreditCard;
import app.repository.interfaces.BillingDetailsDAO;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public class BillingDetailsDAOImpl implements BillingDetailsDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Transactional
    @Override
    public void save(BillingDetails billingDetails) {
        sessionFactory.getCurrentSession().save(billingDetails);
    }

    @Transactional
    @Override
    public void save(BankAccount bankAccount) {
        sessionFactory.getCurrentSession().save(bankAccount);
    }

    @Transactional
    @Override
    public void save(CreditCard creditCard) {
        sessionFactory.getCurrentSession().save(creditCard);
    }

    @SuppressWarnings("unchecked")
    @Transactional(readOnly = true)
    @Override
    public List<BillingDetails> getBillingDetailsByBuyerID(Long id) {
        return sessionFactory
                .getCurrentSession()
                .createQuery("from BillingDetails bd where bd.buyer.id=:id")
                .setParameter("id", id)
                .getResultList();
    }
}
