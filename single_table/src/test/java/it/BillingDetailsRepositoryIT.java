package it;

import app.entity.BankAccount;
import app.entity.BillingDetails;
import app.entity.Buyer;
import app.entity.CreditCard;
import app.repository.interfaces.BillingDetailsDAO;
import app.repository.interfaces.BuyerDAO;
import config.TestConfig;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import java.util.ArrayList;
import java.util.List;

@SpringJUnitConfig(TestConfig.class)
public class BillingDetailsRepositoryIT {

    @Autowired
    private BillingDetailsDAO billingDetailsDAO;

    @Autowired
    private BuyerDAO buyerDAO;

    @Test
    public void test() {
        Buyer buyer = new Buyer();
        buyer.setFirstName("Vladislav");
        buyer.setLastName("Zauholnyi");
        buyerDAO.save(buyer);

        CreditCard creditCard = new CreditCard();
        creditCard.setCardNumber("23091991");
        creditCard.setExpYear(2025);
        creditCard.setExpMonth(9);
        creditCard.setBuyer(buyer);
        billingDetailsDAO.save(creditCard);

        BankAccount bankAccount = new BankAccount();
        bankAccount.setAccount("Vlad's account");
        bankAccount.setBankName("Vlad's bank");
        bankAccount.setBuyer(buyer);
        billingDetailsDAO.save(bankAccount);

        List<BillingDetails> expectedBillingDetails = new ArrayList<>();
        expectedBillingDetails.add(creditCard);
        expectedBillingDetails.add(bankAccount);

        List<BillingDetails> actualBillingDetails = billingDetailsDAO.getBillingDetailsByBuyerID(buyer.getId());

        Assertions.assertEquals(expectedBillingDetails, actualBillingDetails);
    }
}
